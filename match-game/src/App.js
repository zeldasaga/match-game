import './App.css';

function App() {
  const StarMatch = () => {
    const stars = 5;
    const keypad = 9;
    return (
      <div className="game">
        <div className="help">
          Pick one or more numbers that sum to the number of stars
        </div>
        <div className="body">
          <div className="left">
            <div className="start" />
            {utils.range(1, stars).map(starId =>
              <div key={starId} className="star" />)}
          </div>
          <div className="right">
            {utils.range(1, keypad).map(buttonId =>
              <button key={buttonId} className="number">{buttonId}</button>)}
          </div>
        </div>
        <div className="timer">Time Remaining: 10</div>
      </div>
    );
  };

  const colors = {
    available: 'lightgray',
    used: 'lightgreen',
    wrong: 'lightcoral',
    candidate: 'deepskyblue',
  };

  const utils = {
    // Sum an array
    sum: arr => arr.reduce((acc, curr) => acc +curr, 0),

    range: (min, max) => Array.from({ length: max - min + 1 }, (_, i) => min + i),

    random: (min, max) => min + Math.floor(max * Math.random()),

    // Given an array of numbers and a max...
    // Pick a random sum (< max) from the set of all available sums in array
  }

  return (
    <div className="App">
      <StarMatch />
    </div>
  );
}

export default App;
